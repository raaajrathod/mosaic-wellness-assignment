let swiper = undefined;

$(function () {
  console.log("Document Loaded");
  $(".sidenav").sidenav();
  bindClickEvents();
  appendCardMarkup();
  initializeSwiper();
});

const initializeSwiper = function () {
  swiper = new Swiper(".swiper-container", {
    // Optional parameters
    direction: "horizontal",
    loop: false,
    slidesPerView: 3,
    breakpoints: {
      // when window width is >= 320px
      320: {
        slidesPerView: 1,
        spaceBetween: 100,
      },
      // when window width is >= 480px
      480: {
        slidesPerView: 2,
        spaceBetween: 30,
      },
      // when window width is >= 640px
      640: {
        slidesPerView: 3,
        // spaceBetween: 100,
      },
    },
    // freeMode: true,
  });

  swiper.on("reachEnd", function () {
    console.log("End");
    $(".next-slide").addClass("disabled");
  });

  swiper.on("reachBeginning", function () {
    console.log("Beginning");
    $(".prev-slide").addClass("disabled");
  });

  swiper.on("slideChange", function () {
    console.log("Slide Change");

    if (swiper.isEnd) {
      $(".next-slide").addClass("disabled");
      return;
    }

    if (swiper.isBeginning) {
      $(".prev-slide").addClass("disabled");
      return;
    }

    $(".prev-slide").removeClass("disabled");
    $(".next-slide").removeClass("disabled");
  });
};

const bindClickEvents = () => {
  $(".tab_item").click(function (e) {
    $(".tab_item").removeClass("active");

    $(this).addClass("active");
  });

  $(".prev-slide").click(function () {
    swiper.slidePrev(500, false);
  });
  $(".next-slide").click(function () {
    swiper.slideNext(500, false);
  });
};

const appendCardMarkup = () => {
  let markup = ``;

  for (let i = 0; i < 5; i++) {
    markup =
      markup +
      `<div class="swiper-slide"><div class="col"><div class="card"><div class="card-image"> <img src="assets/img/card_img.png"></div><div class="card-content"> <span class="card-title">Hair Fall Control Shampoo</span><div class="for-section"><p class="title"> For</p><p class="for-content"> Hair Fall Reduction</p></div><div class="with-section"><p class="title"> With</p><p class="with-content"> keratine</p><p class="with-content"> Biotin</p></div><div class="price-section"> <span class="price"> Rs. 399</span> <span class="actual-price"> <strike>1194</strike></span> <span class='discount-percent'> 17% OFF</span></div></div><div class="card-action"><div class="card-action-btn center add-to-cart-btn"> <a href="#" class="black-text" style="margin:0px;">Add to Cart</a></div><div class="card-action-btn center buy-now-btn"> <a href="#" class="white-text">Buy Now</a></div></div></div></div></div>`;
  }

  $(".swiper-wrapper").html(markup);
};
